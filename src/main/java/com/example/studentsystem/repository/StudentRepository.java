package com.example.studentsystem.repository;

import com.example.studentsystem.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Integer>{

    /**
     * 按名字查询
     *
     * @param name
     * @return
     */
    List<Student> findByNameLike(String name);
}
