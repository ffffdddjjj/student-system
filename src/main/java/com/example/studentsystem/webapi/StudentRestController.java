package com.example.studentsystem.webapi;


import com.example.studentsystem.domain.Student;
import com.example.studentsystem.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/webapi/student1")
public class StudentRestController {
    @Autowired
    private StudentService studentService;

    /**
     * 读取的url:/webapi/student1/list
     * @return
     */
    @GetMapping("/list")
    public List<Student> getAll(){
        List<Student> students=studentService.findAll();
        return students;
    }
    /**
     * 读取的url:/webapi/student1/get/id
     * @return
     */
    @GetMapping("/get/{id}")
    public Student get(@PathVariable Integer id){
        Student student1=studentService.getById(id);
        student1.setPassword("");
        return student1;
    }
    /**
     * 新增学生的方法/webapi/student1/insert
     * @param student1
     * @return
     */
    @PostMapping("/insert")
    public Student insert(Student student1){
        Student student2=studentService.insert(student1);
        return student2;
    }
    /**
     * 更新学生的方法/webapi/student1/update
     * @param student1
     * @return
     */
    @PostMapping("/update")
    public Student update(Student student1){
        //读取旧的数据
        Student oldstudent=studentService.getById(student1.getId());
        if(ObjectUtils.isEmpty(student1.getPassword())){
            student1.setPassword(oldstudent.getPassword());
        }
        student1=studentService.update(student1);
        return student1;
    }
    /**
     * 删除学生的方法"/webapi/student1/delete/"+id
     * @param id
     * @return
     */
    @DeleteMapping ("/delete/{id}")
//    public void delete(Student student){
//        studentService.delete(student);
//    }
    public void delete1(@PathVariable Integer id){
        studentService.delete(id);
    }
}
