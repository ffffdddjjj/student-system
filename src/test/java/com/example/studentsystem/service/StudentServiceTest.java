package com.example.studentsystem.service;

import com.example.studentsystem.domain.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class StudentServiceTest {

    @Autowired
    private StudentService studentService;

    @Test
    void findAll() {

        List<Student> students= studentService.findAll();

        assertNotNull(students);
    }
    @Test
    void findById() {

        Student student= studentService.findById(1);

        assertNotNull(student);
    }
    @Test
    void insert() {

        Student student=new Student();
        student.setName("李四");
        student.setNo(2020211201);
        student.setPassword("01");
        student.setSex(1);
        student.setAge(22);

        studentService.insert(student);

        assertNotNull(student.getNo());
    }
    @Test
    void update() {

        List<Student> students=studentService.findByName("李四%");
        Student student=students.get(0);
        student.setName("李四1");


        studentService.update(student);

        assertEquals(student.getName(),"李四1");
    }
//    @Test
//    void delete() {
//        studentService.delete(6);
//        Student teacher=studentService.findById(6);
//        assertNotNull(teacher);
//    }
    @Test
    void findByNameLike() {

        List<Student> students= studentService.findByName("董%");

        assertNotNull(students);
    }
}